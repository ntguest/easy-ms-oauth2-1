package com.sgdbf.impulse.security.config.sso;

import com.google.common.collect.Maps;
import com.sgdbf.impulse.common.test.AbstractBaseTest;
import com.sgdbf.impulse.iam.domain.*;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.service.IamService;
import com.sgdbf.impulse.security.service.ImpulseUserDetails;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

/**
 * @author yfo.
 */
@RunWith(SpringRunner.class)
@SpringBootTest("ssoPrincipalExtractor")
public class SSOPrincipalExtractorTest extends AbstractBaseTest {

    @Inject
    private SSOPrincipalExtractor principalExtractor;
    @SpyBean
    private IamService iamService;

    @Before
    public void setUp() {
        super.setUp();
        RealmContext.setCurrentRealm("default");
    }

    @Test
    public void should_extract_principal_when_user_is_an_atc() {

        doReturn(Optional.of(getSearchUserResult())).when(iamService).getUserResult(anyString());
        doReturn(Optional.of(getUserDetail())).when(iamService).getUserDetails(anyString());
        doReturn(Optional.of(new RegionDetail("867"))).when(iamService).getRegion(anyString());
        ImpulseUserDetails userDetails = (ImpulseUserDetails) principalExtractor.extractPrincipal(getUserInfosMap());
        assertThat(userDetails.getUserId()).isEqualTo("SGID_ATC@default");
        assertThat(userDetails.getUsername()).isEqualTo("SGID_ATC");
        assertThat(userDetails.getAccountId()).isEqualTo("SGID_ATC");
        assertThat(userDetails.getWebsites()).containsExactly("default");
        assertThat(userDetails.getRoles()).containsExactly("ADMIN_ATC");
    }

    @Test
    public void should_thrown_exception_when_extract_principal_and_user_is_not_found_in_iam() {
        doReturn(Optional.empty()).when(iamService).getUserResult(anyString());
        Throwable throwable = catchThrowable(() -> principalExtractor.extractPrincipal(getUserInfosMap()));
        assertThat(throwable).isNotNull();
    }

    @Test
    public void should_thrown_exception_when_extract_principal_and_user_details_is_not_found_in_iam() {
        doReturn(Optional.empty()).when(iamService).getUserDetails(anyString());
        Throwable throwable = catchThrowable(() -> principalExtractor.extractPrincipal(getUserInfosMap()));
        assertThat(throwable).isNotNull();
    }

    @Test
    public void should_thrown_exception_when_extract_principal_and_user_is_not_authorized() {
        doReturn(Optional.of(getUserDetail())).when(iamService).getUserDetails(anyString());
        Throwable throwable = catchThrowable(() -> principalExtractor.extractPrincipal(getUserInfosMap()));
        assertThat(throwable).isNotNull();
    }

    private UserDetail getUserDetail() {
        Region region = new Region();
        region.setId("867");
        UserDetail userDetail = new UserDetail();
        userDetail.setRegion(region);
        return userDetail;
    }

    private SearchUserResult getSearchUserResult() {
        SearchUserResult result = new SearchUserResult();
        Row row = new Row();
        row.setId("1");
        Contact contact = new Contact();
        contact.setRows(Lists.newArrayList(row));
        contact.setTotal(1);
        result.setPerson(contact);
        return result;
    }

    private Map<String, Object> getUserInfosMap() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("sub", "SGID_ATC");
        return map;
    }
}
