package com.sgdbf.impulse.security.service;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.common.test.AbstractBaseTest;
import com.sgdbf.impulse.security.config.RealmContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * @author yfo.
 */
@RunWith(SpringRunner.class)
@SpringBootTest("impulseUserDetailsService")
public class ImpulseUserDetailsServiceTest extends AbstractBaseTest {

    @Inject
    private ImpulseUserDetailsService impulseUserDetailsService;
    @Inject
    private OAuthUserService oAuthUserService;
    @Inject
    private BCryptPasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        super.setUp();
        RealmContext.setCurrentRealm("default");
    }

    @Test
    public void should_load_user_by_username() {
        ImpulseUserDetails userDetails = (ImpulseUserDetails) impulseUserDetailsService.loadUserByUsername("admin_dod");
        assertThat(userDetails.getUserId()).isEqualTo("1002@default");
        assertThat(userDetails.getUsername()).isEqualTo("1002");
        assertThat(userDetails.getAccountId()).isEqualTo("1002");
        assertThat(passwordEncoder.matches("password", userDetails.getPassword())).isTrue();
        assertThat(userDetails.getWebsites()).containsExactly("default");
        assertThat(userDetails.getRoles()).containsExactly("ADMIN_DOD", "ADMIN_ATC");
    }

    @Test
    public void should_throw_bad_credentials_when_load_user_by_username() {
        Throwable throwable = catchThrowable(() -> impulseUserDetailsService.loadUserByUsername("unknown_user"));
        assertThat(throwable.getMessage()).isEqualTo(ImpulseMessage.bad_credentials.getKey());
    }
}