package com.sgdbf.impulse.security.service;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.common.test.AbstractBaseTest;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.domain.OAuthUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.util.JsonParserFactory;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.sgdbf.impulse.security.utils.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.oauth2.provider.token.AccessTokenConverter.JTI;

/**
 * @author yfo.
 */
@RunWith(SpringRunner.class)
@SpringBootTest("impersonationService")
public class ImpersonationServiceTest extends AbstractBaseTest {

    @Inject
    private ImpersonationService impersonationService;
    @Inject
    private OAuthUserService oAuthUserService;

    @Before
    public void setUp() {
        super.setUp();
        RealmContext.setCurrentRealm("default");
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("VI_1234", null));
    }

    @Test
    public void should_get_impersonated_oauth2_access_token_with_account_id_and_default_user() {
        DefaultOAuth2AccessToken accessToken = impersonationService.getImpersonatedOAuth2AccessToken(null, "clim", "VI_9999", Lists.newArrayList("PERM_REGISTER"));
        assertThat((String) accessToken.getAdditionalInformation().get(JTI)).isNotEmpty();
        assertThat(accessToken.getRefreshToken()).isNull();
        String idToken = (String) accessToken.getAdditionalInformation().get(ID_TOKEN);
        Map<String, Object> claims = JsonParserFactory.create().parseMap(JwtHelper.decode(idToken).getClaims());
        assertThat(claims.size()).isEqualTo(9);
        assertThat(claims.get(SUB)).isEqualTo("VI_9999@clim");
        assertThat(claims.get(ACCOUNT_ID)).isEqualTo("VI_9999");
        assertThat(claims.get(LOGIN)).isNull();
        assertThat(claims.get(FIRST_NAME)).isNull();
        assertThat(claims.get(LAST_NAME)).isNull();
        assertThat(claims.get(IMPERSONATOR)).isEqualTo(true);
        assertThat(claims.get(SG_ID)).isEqualTo("VI_1234");
        assertThat((List<String>) claims.get(ROLES)).containsExactly("PERM_REGISTER");
        assertThat((List<String>) claims.get(SITES)).containsExactly("clim");
    }

    @Test
    public void should_get_impersonated_oauth2_access_token_with_account_id_and_existing_user() {
        RealmContext.setCurrentRealm("cedeo");
        OAuthUser user = oAuthUserService.findOAuthUserByIdOrLoginAndWebsitesId("VI_1328", Lists.newArrayList("cedeo")).get();
        DefaultOAuth2AccessToken accessToken = impersonationService.getImpersonatedOAuth2AccessToken(user, "cedeo", "VI_1328", Lists.newArrayList("PERM_REGISTER"));
        assertThat((String) accessToken.getAdditionalInformation().get(JTI)).isNotEmpty();
        assertThat(accessToken.getRefreshToken()).isNull();
        String idToken = (String) accessToken.getAdditionalInformation().get(ID_TOKEN);
        Map<String, Object> claims = JsonParserFactory.create().parseMap(JwtHelper.decode(idToken).getClaims());
        assertThat(claims.size()).isEqualTo(9);
        assertThat(claims.get(SUB)).isEqualTo("VI_1328@cedeo");
        assertThat(claims.get(ACCOUNT_ID)).isEqualTo("VI_1328");
        assertThat(claims.get(LOGIN)).isEqualTo("eloenz@sfr.fr");
        assertThat(claims.get(FIRST_NAME)).isEqualTo("eloenz_firstname");
        assertThat(claims.get(LAST_NAME)).isEqualTo("eloenz_lastname");
        assertThat(claims.get(IMPERSONATOR)).isEqualTo(true);
        assertThat(claims.get(SG_ID)).isEqualTo("VI_1234");
        assertThat((List<String>) claims.get(ROLES)).containsExactly("ACCOUNT");
        assertThat((List<String>) claims.get(SITES)).containsExactly("cedeo");
    }
}