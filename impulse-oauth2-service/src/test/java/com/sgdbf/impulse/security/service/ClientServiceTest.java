package com.sgdbf.impulse.security.service;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.common.test.AbstractBaseTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * @author yfo.
 */
@RunWith(SpringRunner.class)
@SpringBootTest("clientService")
public class ClientServiceTest extends AbstractBaseTest {

    @Inject
    private ClientService clientService;

    @Test
    public void should_throw_access_denied_exception_when_client_does_not_exist() {
        Throwable throwable = catchThrowable(() -> clientService.loadClientByClientId("unknown_client"));
        assertThat(throwable.getMessage()).isEqualTo(ImpulseMessage.access_denied.getKey());
    }

    @Test
    public void should_throw_access_denied_exception_when_client_is_not_enabled() {
        Throwable throwable = catchThrowable(() -> clientService.loadClientByClientId("test"));
        assertThat(throwable.getMessage()).isEqualTo(ImpulseMessage.access_denied.getKey());
    }
}