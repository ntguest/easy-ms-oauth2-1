package com.sgdbf.impulse.security.config.sso;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A custom authentication success handler to override redirectStrategy using zuul instead of oauth2 as redirect url
 *
 * @author yfo.
 */
@Component
@RequiredArgsConstructor
public class SSOSimpleUrlAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final SSODefaultRedirectStrategy redirectStrategy;

    @PostConstruct
    public void init() {
        this.setRedirectStrategy(redirectStrategy);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        super.onAuthenticationSuccess(request, response, authentication);
    }
}
