package com.sgdbf.impulse.security.converter;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.dto.OAuthUserRequest;
import lombok.Data;

import java.util.function.Function;

import static com.sgdbf.impulse.security.utils.Constants.AROBASE;

/**
 * @author sla.
 */
@Data(staticConstructor = "newInstance")
public class OAuthUserRequestConverter implements Function<OAuthUserRequest, OAuthUser> {

    private String websiteId;

    @Override
    public OAuthUser apply(OAuthUserRequest input) {
        if (input == null) {
            return null;
        }
        OAuthUser result = new OAuthUser();
        result.setId(input.getId() + AROBASE + websiteId);
        result.setAccountId(input.getId());
        result.setLogin(input.getLoginLowerCase());
        result.setWebSites(Lists.newArrayList(websiteId));
        result.setPassword(input.getPassword());
        result.setEmailValidation(input.getEmailValidation());
        result.setIsActive(input.getIsActive());
        result.setRoles(input.getRoles());
        return result;
    }

    public OAuthUserRequestConverter withWebsiteId(String websiteId) {
        this.websiteId = websiteId;
        return this;
    }
}
