package com.sgdbf.impulse.security.rabbitmq;

/**
 * @author yfo.
 */
public interface RemoteService<U> {

    void syncUserCommand(String websiteId, U oAuthUser);
}
