package com.sgdbf.impulse.security.rabbitmq.listener;

import com.sgdbf.impulse.rabbitmq.EntityProcessor;
import com.sgdbf.impulse.rabbitmq.PojoWrapper;
import com.sgdbf.impulse.security.service.OAuthUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author yfo.
 */
@Slf4j
@AllArgsConstructor
@Component
public class OAuthUserRequestIdsProcessor implements EntityProcessor<List<String>> {

    private final OAuthUserService oAuthUserService;

    @Override
    public void process(String websiteId, PojoWrapper<List<String>> pojoWrapper) {
        log.info("Processing request received on rabbitmq {} with event {} and websiteId {}", pojoWrapper.getPojo(), pojoWrapper.getEventType(), websiteId);
        oAuthUserService.delete(websiteId, pojoWrapper.getPojo());
    }
}
