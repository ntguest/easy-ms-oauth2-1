package com.sgdbf.impulse.security.repository.criterias;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.security.config.RealmContext;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

import static com.sgdbf.impulse.security.utils.Constants.*;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * @author yfo.
 */
@NoArgsConstructor
public final class OAuthCriteriaBuilder {

    private List<Criteria> criterias = Lists.newArrayList();
    private Criteria criteria = new Criteria();

    public OAuthCriteriaBuilder withLogin(String login) {
        criterias.add(new Criteria().orOperator(Criteria.where(LOGIN).is(login.toLowerCase()), Criteria.where(ID).is(getCombinedLogin(login))));
        return this;
    }

    public OAuthCriteriaBuilder withWebsiteIds(List<String> websiteIds) {
        criterias.add(Criteria.where(WEBSITES).in(websiteIds));
        return this;
    }

    public Criteria build() {
        return !isEmpty(criterias) ? criteria.andOperator(criterias.toArray(new Criteria[criterias.size()])) : criteria;
    }

    private String getCombinedLogin(String id) {
        return id.contains(AROBASE) ? id : (id + AROBASE + RealmContext.getCurrentRealm());
    }
}
