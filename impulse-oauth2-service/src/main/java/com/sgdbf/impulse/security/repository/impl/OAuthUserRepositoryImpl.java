package com.sgdbf.impulse.security.repository.impl;

import com.sgdbf.impulse.common.ms.mongo.GenericRepository;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.repository.IOAuthUserRepository;
import com.sgdbf.impulse.security.repository.criterias.OAuthCriteriaBuilder;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;
import java.util.Optional;

/**
 * @author yfo.
 */
@AllArgsConstructor
public class OAuthUserRepositoryImpl implements IOAuthUserRepository {

    private final GenericRepository repository;

    @Override
    public Optional<OAuthUser> findOAuthUserByIdOrLoginAndWebsitesId(String id, List<String> webSites) {
        Criteria criteria = new OAuthCriteriaBuilder().withLogin(id).withWebsiteIds(webSites).build();
        return Optional.ofNullable(repository.getOneResult(criteria, OAuthUser.class));
    }
}
