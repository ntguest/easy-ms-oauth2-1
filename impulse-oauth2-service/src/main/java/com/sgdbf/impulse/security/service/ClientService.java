package com.sgdbf.impulse.security.service;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.security.config.OAuthProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

/**
 * @author yfo.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ClientService implements ClientDetailsService {

    private final OAuthProperties properties;
    private final OAuthUserService oAuthUserService;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        OAuthProperties.OAuthClientDetails oAuthClientDetails = properties.getOAuthClients().get(clientId);
        if (Objects.isNull(oAuthClientDetails)) {
            log.error("No client with requested id " + clientId);
            throw new AccessDeniedException(ImpulseMessage.access_denied.getKey());
        }

        if (!oAuthClientDetails.isEnabled()) {
            log.error("Client {} is disabled", clientId);
            throw new AccessDeniedException(ImpulseMessage.access_denied.getKey());
        }

        BaseClientDetails clientDetails = new BaseClientDetails();
        clientDetails.setClientId(clientId);
        clientDetails.setClientSecret(oAuthClientDetails.getClientSecret());
        clientDetails.setScope(Collections.singletonList("Impulse"));
        clientDetails.setAuthorities(oAuthUserService.findRolesWithPermissions(oAuthClientDetails.getAuthorities()));
        clientDetails.setAutoApproveScopes(Collections.singletonList("Impulse"));
        clientDetails.setAuthorizedGrantTypes(oAuthClientDetails.getAuthorizedGrantTypes());
        clientDetails.setRegisteredRedirectUri(oAuthClientDetails.getRegisteredRedirectUris());
        Optional.ofNullable(oAuthClientDetails.getAccessTokenValidity()).ifPresent(clientDetails::setAccessTokenValiditySeconds);
        Optional.ofNullable(oAuthClientDetails.getRefreshTokenValidity()).ifPresent(clientDetails::setRefreshTokenValiditySeconds);

        log.debug("Load Client by client id {}, {} " + clientId, clientDetails.toString());
        return clientDetails;
    }
}
