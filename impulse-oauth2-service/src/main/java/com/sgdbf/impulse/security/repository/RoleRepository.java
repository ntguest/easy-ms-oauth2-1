package com.sgdbf.impulse.security.repository;

import com.sgdbf.impulse.security.domain.Role;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author obe.
 */
@Component
public interface RoleRepository extends MongoRepository<Role, String> {

    List<Role> findByIdIn(List<String> ids);
}
