package com.sgdbf.impulse.security.config;

import com.sgdbf.impulse.security.service.ImpulseUserDetailsService;
import com.sgdbf.impulse.security.service.OAuthUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author obe.
 */
@Configuration
@RequiredArgsConstructor
public class DaoAuthenticationProviderConfiguration {

    private final ImpulseUserDetailsService impulseUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final OAuthUserService oAuthUserService;

    @Bean
    @Primary
    public ImpulseDaoAuthenticationProvider impulseDaoAuthenticationProvider() {
        ImpulseDaoAuthenticationProvider impulseDaoAuthenticationProvider = new ImpulseDaoAuthenticationProvider(oAuthUserService, passwordEncoder);
        impulseDaoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
        impulseDaoAuthenticationProvider.setUserDetailsService(impulseUserDetailsService);
        return impulseDaoAuthenticationProvider;
    }
}
