package com.sgdbf.impulse.security.config;

import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.rabbitmq.OAuthRemoteService;
import com.sgdbf.impulse.security.service.ImpulseUserDetails;
import com.sgdbf.impulse.security.service.OAuthUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

/**
 * @author yfo.
 */
@Slf4j
@Component
@AllArgsConstructor
public class AuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {

    private final OAuthUserService oAuthUserService;
    private final OAuthRemoteService oAuthRemoteService;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        if (isNotImpersonator(event.getAuthentication())) {
            String realm = RealmContext.getCurrentRealm();
            User user = (User) event.getAuthentication().getPrincipal();
            Optional<OAuthUser> oAuthUserOpt = oAuthUserService.findOAuthUserByIdOrLoginAndWebsitesId(user.getUsername(), Collections.singletonList(realm));
            oAuthUserOpt.ifPresent(o -> oAuthRemoteService.syncUserCommand(realm, o));
        }
    }

    // we should not update user statistics when it is an impersonator
    private boolean isNotImpersonator(Authentication authentication) {
        return authentication.getPrincipal() instanceof ImpulseUserDetails && !((ImpulseUserDetails) authentication.getPrincipal()).isImpersonator();
    }
}