package com.sgdbf.impulse.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.sgdbf.impulse.security.config.sso.JwtBearerTokenGranter;
import com.sgdbf.impulse.security.service.ClientService;
import com.sgdbf.impulse.security.service.ImpulseUserDetailsService;
import com.sgdbf.impulse.security.utils.TokenHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.security.KeyPair;
import java.util.Collections;
import java.util.List;

/**
 * @author yfo.
 */
@Slf4j
@Configuration
@EnableResourceServer
@ComponentScan(basePackages = {"com.sgdbf.impulse.rabbitmq", "com.sgdbf.impulse.common"})
@EnableMongoRepositories(basePackages = "com.sgdbf.impulse.security.repository")
@EnableAuthorizationServer
@RequiredArgsConstructor
public class OAuthConfiguration extends AuthorizationServerConfigurerAdapter {

    private final AuthenticationManager authManager;
    private final ClientService clientService;
    private final ImpulseUserDetailsService impulseUserDetailsService;
    private final OAuthProperties properties;
    private final TokenHelper tokenHelper;
    private final KeyPair keyPair;
    private final ObjectMapper objectMapper;
    @Value("${impulse.gateway.lan.public.key}")
    private String publicKey;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientService);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.tokenServices(tokenServices()).authenticationManager(authManager).tokenGranter(tokenGranter(endpoints));
    }

    private TokenGranter tokenGranter(final AuthorizationServerEndpointsConfigurer endpoints) {
        List<TokenGranter> granters = Lists.newArrayList(Collections.singletonList(endpoints.getTokenGranter()));
        granters.add(new JwtBearerTokenGranter(endpoints, objectMapper, publicKey));
        return new CompositeTokenGranter(granters);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security
                .allowFormAuthenticationForClients()
                .realm("realm")
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()");
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore());
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setReuseRefreshToken(false);
        tokenServices.setClientDetailsService(clientService);
        tokenServices.setTokenEnhancer(jwtAccessTokenConverter());
        tokenServices.setAccessTokenValiditySeconds(properties.getToken().getExpirationTime());
        return tokenServices;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        CustomJwtAccessTokenConverter customJwtAccessTokenConverter = new CustomJwtAccessTokenConverter();
        customJwtAccessTokenConverter.setTokenHelper(tokenHelper);
        customJwtAccessTokenConverter.setKeyPair(keyPair);
        DefaultAccessTokenConverter defaultAccessTokenConverter = new DefaultAccessTokenConverter();
        defaultAccessTokenConverter.setUserTokenConverter(userAuthenticationConverter());
        customJwtAccessTokenConverter.setAccessTokenConverter(defaultAccessTokenConverter);
        return customJwtAccessTokenConverter;
    }

    @Bean
    public CustomUserAuthenticationConverter userAuthenticationConverter() {
        CustomUserAuthenticationConverter userAuthenticationConverter = new CustomUserAuthenticationConverter();
        userAuthenticationConverter.setUserDetailsService(impulseUserDetailsService);
        return userAuthenticationConverter;
    }
}



