package com.sgdbf.impulse.security.config.sso;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author yfo.
 */
@Slf4j
@Component
public class SSODefaultRedirectStrategy extends DefaultRedirectStrategy {

    @Value("${impulse.gateway.oauth2.url}")
    private String gatewayUrl;

    /**
     * Redirects the response to the supplied URL prefixed by the zuul url.
     */
    @Override
    public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
        String redirectUrl = calculateRedirectUrl(request.getContextPath(), url);
        redirectUrl = response.encodeRedirectURL(redirectUrl);

        String gatewayRedirectUrl = buildGatewayRedirectUrl(redirectUrl);

        if (logger.isDebugEnabled()) {
            logger.debug("Redirecting to '" + gatewayRedirectUrl + "'");
        }
        response.sendRedirect(gatewayRedirectUrl);
    }

    private String buildGatewayRedirectUrl(String redirectUrl) {
        try {
            return gatewayUrl + new URL(redirectUrl).getFile();
        } catch (MalformedURLException e) {
            log.error("Exception while get build gateway redirectUrl" + redirectUrl, e.getMessage());
            return redirectUrl;
        }
    }
}
