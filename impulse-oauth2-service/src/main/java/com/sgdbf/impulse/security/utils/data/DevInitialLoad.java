package com.sgdbf.impulse.security.utils.data;

import com.sgdbf.impulse.common.ms.mongo.DataLoader;
import com.sgdbf.impulse.common.ms.mongo.InitialLoad;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.domain.Permission;
import com.sgdbf.impulse.security.domain.Role;
import com.sgdbf.impulse.security.repository.OAuthUserRepository;
import com.sgdbf.impulse.security.repository.PermissionRepository;
import com.sgdbf.impulse.security.repository.RoleRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author yfo.
 */
@Slf4j
@Component
@AllArgsConstructor
@Profile({"mongo", "test", "mock"})
public class DevInitialLoad implements InitialLoad {

    private final OAuthUserRepository oAuthUserRepository;
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;
    private final DataLoader loader;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public void start() {
        permissionRepository.deleteAll();
        roleRepository.deleteAll();
        oAuthUserRepository.deleteAll();
        permissionRepository.save(loader.load("permissions.json", Permission.class));
        roleRepository.save(loader.load("roles.json", Role.class));
        List<OAuthUser> oAuthUsers = loader.load("oauth-users.json", OAuthUser.class);
        oAuthUsers.forEach(user -> user.setPassword(passwordEncoder.encode(user.getPassword())));
        oAuthUserRepository.save(oAuthUsers);
    }
}
