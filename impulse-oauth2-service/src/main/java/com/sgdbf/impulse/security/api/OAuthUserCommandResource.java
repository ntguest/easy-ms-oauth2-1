package com.sgdbf.impulse.security.api;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.service.ImpersonationService;
import com.sgdbf.impulse.security.service.OAuthUserService;
import com.sgdbf.impulse.security.service.ValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author sla.
 */
@Slf4j
@RestController
@RequestMapping("/api")
@Validated
@AllArgsConstructor
public class OAuthUserCommandResource {

    private final OAuthUserService oAuthUserService;
    private final ValidationService validationService;
    private final ImpersonationService impersonationService;

    @GetMapping(produces = APPLICATION_JSON_VALUE, path = "/v1/users/me")
    public ResponseEntity<Map<String, Object>> me(@RequestParam String realm, Principal principal) {
        log.info("return authenticated user information by realm {}", realm);
        Map<String, Object> result = oAuthUserService.buildUserInformation(realm, principal);
        return CollectionUtils.isEmpty(result) ? ResponseEntity.badRequest().build() : ResponseEntity.ok(result);
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE, path = "/v1/connect-as")
    @PreAuthorize("hasAuthority('PERM_CONNECTION_AS')")
    public ResponseEntity<OAuth2AccessToken> connectionAs(Principal impersonator, @RequestParam String realm, @RequestParam String accountId, @RequestParam List<String> roles) {
        log.info("connect as {} by realm id {}", accountId, realm);
        Optional<OAuthUser> optionalUser = oAuthUserService.findOAuthUserByIdOrLoginAndWebsitesId(accountId, Lists.newArrayList(realm));
        OAuthUser user = optionalUser.orElse(null);
        validationService.validateImpersonation(impersonator, user, realm);
        DefaultOAuth2AccessToken accessToken = impersonationService.getImpersonatedOAuth2AccessToken(user, realm, accountId, roles);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cache-Control", "no-store");
        headers.set("Pragma", "no-cache");
        return new ResponseEntity<>(accessToken, headers, HttpStatus.OK);
    }
}
