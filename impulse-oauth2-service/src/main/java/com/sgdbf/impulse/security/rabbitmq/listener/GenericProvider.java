package com.sgdbf.impulse.security.rabbitmq.listener;

import com.sgdbf.impulse.common.domain.rabbit.AbstractEntityProvider;
import com.sgdbf.impulse.common.domain.rabbit.FunctionalEntityEnum;
import com.sgdbf.impulse.rabbitmq.EntityProcessor;
import com.sgdbf.impulse.rabbitmq.FunctionalEntity;
import com.sgdbf.impulse.security.dto.OAuthUserRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sla.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class GenericProvider extends AbstractEntityProvider {

    private final OAuthUserRequestProcessor oAuthUserRequestProcessor;
    private final OAuthUserRequestIdsProcessor oAuthUserRequestIdsProcessor;
    private Map<FunctionalEntity, Pair<EntityProcessor, Class>> processorMap;

    @PostConstruct
    public void initProcessorMap() {
        processorMap = new HashMap<FunctionalEntity, Pair<EntityProcessor, Class>>() {{
            put(FunctionalEntityEnum.OAuth, Pair.of(oAuthUserRequestProcessor, OAuthUserRequest.class));
            put(FunctionalEntityEnum.OAuthIds, Pair.of(oAuthUserRequestIdsProcessor, List.class));
        }};
    }

    @Override
    public Class getClass(FunctionalEntity functionalEntity) {
        return processorMap.getOrDefault(functionalEntity, null).getRight();
    }

    @Override
    public EntityProcessor getProcessor(FunctionalEntity functionalEntity) {
        return processorMap.getOrDefault(functionalEntity, null).getLeft();
    }
}
