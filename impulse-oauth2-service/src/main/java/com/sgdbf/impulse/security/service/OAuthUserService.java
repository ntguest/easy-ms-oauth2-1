package com.sgdbf.impulse.security.service;

import com.google.common.collect.Maps;
import com.sgdbf.impulse.common.ms.utils.StreamUtils;
import com.sgdbf.impulse.security.converter.OAuthUserRequestConverter;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.domain.Permission;
import com.sgdbf.impulse.security.domain.PermissionType;
import com.sgdbf.impulse.security.domain.Role;
import com.sgdbf.impulse.security.dto.OAuthUserRequest;
import com.sgdbf.impulse.security.repository.OAuthUserRepository;
import com.sgdbf.impulse.security.repository.PermissionRepository;
import com.sgdbf.impulse.security.repository.RoleRepository;
import com.sgdbf.impulse.security.utils.Constants;
import com.sgdbf.impulse.security.utils.TokenHelper;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sla.
 */
@Service
@AllArgsConstructor
public class OAuthUserService {

    private final OAuthUserRepository oAuthUserRepository;
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;
    private final TokenHelper tokenHelper;

    public OAuthUser save(String websiteId, OAuthUserRequest oAuthUserRequest) {
        return oAuthUserRepository.save(OAuthUserRequestConverter.newInstance().withWebsiteId(websiteId).apply(oAuthUserRequest));
    }

    public OAuthUser update(OAuthUser oAuthUser) {
        return oAuthUserRepository.save(oAuthUser);
    }

    public void delete(String websiteId, List<String> ids) {
        oAuthUserRepository.deleteByIdIn(ids.stream().map(id -> String.join(Constants.AROBASE, id, websiteId)).collect(Collectors.toList()));
    }

    public Optional<OAuthUser> findOAuthUserByIdOrLoginAndWebsitesId(String id, List<String> websiteIds) {
        return oAuthUserRepository.findOAuthUserByIdOrLoginAndWebsitesId(id, websiteIds);
    }

    public Map<String, Object> buildUserInformation(String realm, Principal principal) {
        OAuth2Authentication authentication = (OAuth2Authentication) principal;
        if (Objects.isNull(authentication.getUserAuthentication())) {
            return Maps.newHashMap();
        }
        ImpulseUserDetails impulseUserDetails = (ImpulseUserDetails) authentication.getUserAuthentication().getPrincipal();
        return impulseUserDetails.getWebsites().contains(realm) ? tokenHelper.buildUserInformation(impulseUserDetails) : Maps.newHashMap();
    }

    public List<String> findReadPermissions(List<String> rolesIds) {
        return StreamUtils.ofNullable(roleRepository.findByIdIn(rolesIds))
                .filter(Objects::nonNull)
                .map(this::getReadPermissionsFromRole)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<GrantedAuthority> findRolesWithPermissions(List<String> roles) {
        return tokenHelper.createAuthorityList(roles, findPermissionsByRoles(roles));
    }

    public List<GrantedAuthority> findRolesWithReadPermissions(List<String> roles) {
        return tokenHelper.createAuthorityList(roles, findReadPermissions(roles));
    }

    public List<String> findPermissionsByRoles(List<String> rolesIds) {
        return StreamUtils.ofNullable(roleRepository.findByIdIn(rolesIds))
                .filter(Objects::nonNull)
                .map(this::getPermissionsFromRole)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    private List<String> getPermissionsFromRole(Role role) {
        List<String> permissions = role.getPermissions();
        return Stream.concat(StreamUtils.ofNullable(role.getAcl()), StreamUtils.ofNullable(permissions)).collect(Collectors.toList());
    }

    private List<String> getReadPermissionsFromRole(Role role) {
        List<Permission> perms = permissionRepository.findByIdInAndType(role.getPermissions(), PermissionType.READ);
        List<String> permissions = perms.stream().map(Permission::getId).collect(Collectors.toList());
        return Stream.concat(StreamUtils.ofNullable(role.getAcl()), StreamUtils.ofNullable(permissions)).collect(Collectors.toList());
    }

    public List<Role> findRolesByIdIn(List<String> roles) {
        return roleRepository.findByIdIn(roles);
    }
}
