package com.sgdbf.impulse.security.repository;

import com.sgdbf.impulse.security.domain.OAuthUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author sla.
 */
@Component
public interface OAuthUserRepository extends MongoRepository<OAuthUser, String>, IOAuthUserRepository {

    void deleteByIdIn(List<String> ids);
}
