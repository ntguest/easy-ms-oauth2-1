package com.sgdbf.impulse.security.repository;

import com.sgdbf.impulse.security.domain.OAuthUser;

import java.util.List;
import java.util.Optional;

/**
 * @author yfo.
 */
public interface IOAuthUserRepository {

    Optional<OAuthUser> findOAuthUserByIdOrLoginAndWebsitesId(String id, List<String> webSites);
}
