package com.sgdbf.impulse.security.rabbitmq.listener;

import com.sgdbf.impulse.rabbitmq.EntityProcessor;
import com.sgdbf.impulse.rabbitmq.PojoWrapper;
import com.sgdbf.impulse.security.dto.OAuthUserRequest;
import com.sgdbf.impulse.security.service.OAuthUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author yfo.
 */
@Slf4j
@AllArgsConstructor
@Component
public class OAuthUserRequestProcessor implements EntityProcessor<OAuthUserRequest> {

    private final OAuthUserService oAuthUserService;

    @Override
    public void process(String websiteId, PojoWrapper<OAuthUserRequest> pojoWrapper) {
        log.info("Processing request received on rabbitmq {} with event {} and websiteId {}", pojoWrapper.getPojo(), pojoWrapper.getEventType(), websiteId);
        oAuthUserService.save(websiteId, pojoWrapper.getPojo());
    }
}
