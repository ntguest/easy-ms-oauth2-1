package com.sgdbf.impulse.security.service;

import com.sgdbf.impulse.common.domain.exception.ImpulseMessage;
import com.sgdbf.impulse.common.ms.rest.Validator;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.domain.Role;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static com.sgdbf.impulse.common.ms.utils.Constants.ACL_ADMIN;

/**
 * @author yfo.
 */
@Component
@RequiredArgsConstructor
public class ValidationService {

    private final OAuthUserService oAuthUserService;

    public void validateImpersonation(Principal impersonator, OAuthUser account, String realm) {
        Validator.of(Pair.of(account, impersonator))
                .ifMatch(account, Objects::nonNull)
                .validateIf(Pair::getLeft, this::isAdmin, () -> new ResourceAccessException(ImpulseMessage.connection_as_admin_not_allowed.getKey()))
                .ifMatch(impersonator, Objects::nonNull)
                .validateIf(Pair::getRight, r -> isNotAllowedToRealm((OAuth2Authentication) r, realm), () -> new ResourceAccessException(ImpulseMessage.connection_as_not_allowed_to_realm.getKey()))
                .execute();
    }

    private boolean isAdmin(OAuthUser user) {
        List<Role> roles = oAuthUserService.findRolesByIdIn(user.getRoles());
        return roles.stream().map(Role::getAcl).filter(Objects::nonNull).flatMap(Collection::stream).anyMatch(acl -> acl.contains(ACL_ADMIN));
    }

    private boolean isNotAllowedToRealm(OAuth2Authentication impersonator, String realm) {
        if (impersonator.getPrincipal() instanceof String) {
            return isClientAuthenticationWithoutAdminAcl(impersonator);
        }
        ImpulseUserDetails principal = (ImpulseUserDetails) impersonator.getUserAuthentication().getPrincipal();
        return !principal.getWebsites().contains(realm);
    }

    private boolean isClientAuthenticationWithoutAdminAcl(OAuth2Authentication impersonator) {
        return !impersonator.getAuthorities().contains(new SimpleGrantedAuthority(ACL_ADMIN));
    }
}
