package com.sgdbf.impulse.security.config.sso;

import com.google.common.collect.Lists;
import com.sgdbf.impulse.security.config.RealmContext;
import com.sgdbf.impulse.security.domain.OAuthUser;
import com.sgdbf.impulse.security.service.IamService;
import com.sgdbf.impulse.security.service.ImpulseUserDetails;
import com.sgdbf.impulse.security.service.OAuthUserService;
import com.sgdbf.impulse.security.utils.TokenHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author yfo.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class SSOPrincipalExtractor implements PrincipalExtractor {

    private final OAuthUserService oAuthUserService;
    private final IamService iamService;
    private final TokenHelper tokenHelper;

    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        String sgId = (String) map.get("sub");
        Optional<OAuthUser> user = oAuthUserService.findOAuthUserByIdOrLoginAndWebsitesId(sgId, Lists.newArrayList(RealmContext.getCurrentRealm()));
        return user.map(u -> buildImpulseUserDetails(u, map)).orElseGet(() -> findUserBySgIdFromIAM(sgId, map));
    }

    private ImpulseUserDetails findUserBySgIdFromIAM(String sgId, Map<String, Object> map) {
        OAuthUser oAuthUser = iamService.getUserDetailBySgId(sgId);
        return buildImpulseUserDetails(oAuthUser, map);
    }

    private ImpulseUserDetails buildImpulseUserDetails(OAuthUser user, Map<String, Object> map) {
        List<GrantedAuthority> authorities = oAuthUserService.findRolesWithPermissions(user.getRoles());
        map.put("authorities", authorities); // to be used by FixedAuthoritiesExtractor to extract authorities
        return tokenHelper.getImpulseUserDetailsByAuthorities(user, authorities);
    }
}
